;(function() {
	// Page transitions using Barba.js
	// 1. Set up barba
	// 2. Start in on DOM ready

	// A simple hide/show transition.
	var HideShowTransition = Barba.BaseTransition.extend({
		start() {
			this.newContainerLoading.then(this.finish.bind(this))
		},
		finish() {
			document.body.scrollTop = 0
			this.done()
		}
	})

	// Choose the transition.
	Barba.Pjax.getTransition = function() {
		return HideShowTransition
	}

	// Do stuff when new page is ready.
	Barba.Dispatcher.on('newPageReady', function(currentStatus) {
		function updateMenu() {
			// Get path of current page. HAS to match the href.
			const link =
				'/' + currentStatus.url.split(window.location.origin)[1].substring(1)
			const nav = document.querySelector('.FixedNav')
			const links = nav.querySelectorAll('a')
			const activeLink = nav.querySelector(`[href="${link}"]`)
			// Remove active link from all.
			Array.prototype.forEach.call(links, function(navigationLink) {
				navigationLink.classList.remove('active')
			})
			// Set active link.
			if (activeLink) {
				activeLink.classList.add('active')
			}
		}

		updateMenu()
	})

	// Barba.Dispatcher.on('linkClicked', function(el, event) {
	// 	console.log(el, event);
	// })

	document.addEventListener('DOMContentLoaded', function() {
		// Start it and enable prefetching.
		Barba.Pjax.start()
		Barba.Prefetch.init()
	})
})()
