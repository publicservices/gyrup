---
title: Råvarene
weight: 2
---

# Spelt
Spelten i øllen er dyrket på Gyrup i Thy.. Spelt er en af de gamle hvedesorter. Det er en smagfuld hvede med et højt proteinindhold og massere af smag. Spelt høstes med skaller. Skallerne hjælper bryggeren til lettere at filtrere urten fra masken.

<img src="/spelt.jpg" alt="Spelt">

## Spelt-bonden
Nicolaj Nicolajsen er økologisk landmand i 7. generation på Gyrup. For tyve år side omlagde Nicolaj gården til økologi og dyrker i dag økologisk korn til blandt andet malt og mel. Gården har 170 økologiske malkekøer og en besætning af Galloway-kvæg, som naturplejer Agger Tange.

<img src="/speltbonde.jpg" alt="Nicolaj">

# Byg
Den økologiske byg er en 2-radet vårbyg af sorten Odyssey, dyrket på to forskellige marker på Gyrup i Thy.  Byg er arbejdshesten på bryggeriet. Det skyldes særligt det høje indhold af enzymerne alpha- og beta-amulase, som kan omdanne stivelse til sukker under mæskningen.  Enzymerne dannes under maltningen af byggen.

<img src="/byg.jpg" alt="Byg">

## Byg-bonden
Nicolaj Nicolajsen er økologisk landmand i 7. generation på Gyrup. For tyve år side omlagde Nicolaj gården til økologi og dyrker i dag økologisk korn til blandt andet malt og mel. Gården har 170 økologiske malkekøer og en besætning af Galloway-kvæg, som naturplejer Agger Tange.

# Hvede
Hveden er dyrket på Kildegårdens marker i Sydthy. Det er en vårhvede af sorten Mariboss, en moderne vårhvede som egner sig godt til økologisk dyrkning. Hvede giver rundhed til øllet og hjælper til at skabe en stabil skumkrone.

<img src="/hvede.jpg" alt="Hvede">

## Hvede-bonden
Knud Erik Sørensen har dyrket vårhveden på sine marker ved den økologiske gård Kildegården i Thy. På gården drives både korn- og grøntsagsavl, ligesom familien har 500 høns, som hver dag leverer friske æg til supermarkederne i Thy.

<img src="/hvedebonde.jpg" alt="Knud Erik">
