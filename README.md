# Gyrup

## How to edit the content

All content is stored in markdown files inside the `content` folder.

## The website

Using [Hugo](http://gohugo.io/getting-started/installing/ ) we can build a static website from the markdown files. Make sure it's installed, then run `hugo server` and visit http://localhost:1313 for a preview. To build the site into the `dist` folder, run `hugo`

## Browser support

We intentionally do not have a build system and still support Safari 9. That means do not use `let`, `const` or `() => {}`. It's fine.

